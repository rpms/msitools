#define _version_suffix

Name:           msitools
Version:        0.98
Release:        1%{?dist}
Summary:        Windows Installer tools

License:        GPLv2+
URL:            http://ftp.gnome.org/pub/GNOME/sources/%{name}
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/%{version}/%{name}-%{version}%{?_version_suffix}.tar.xz

Requires:       libgsf >= 1.14.24-2

BuildRequires:  glib2-devel
BuildRequires:  gobject-introspection-devel
BuildRequires:  intltool
BuildRequires:  libgcab1-devel >= 0.2
BuildRequires:  libgsf-devel
BuildRequires:  libuuid-devel
BuildRequires:  vala-tools

%description
msitools is a collection of utilities to inspect and create Windows
Installer files.  It is useful in a cross-compilation environment such
as fedora-mingw.

%package -n libmsi1
Summary:        A library to manipulate Windows .MSI files
License:        LGPLv2+

%description -n libmsi1
libmsi is a GObject library to work with Windows Installer files.  It is
a port from the MSI library of the Wine project.

%package -n libmsi1-devel
Summary:        A library to manipulate Windows .MSI files
License:        LGPLv2+
Requires:       libmsi1%{?_isa} = %{version}-%{release}

%description -n libmsi1-devel
The libmsi1-devel package includes the header files for libmsi.

%prep
%setup -q -n msitools-%{version}%{?_version_suffix}

%build
%configure --enable-fast-install
make %{?_smp_mflags} V=1


%install
%make_install

rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la

%find_lang %{name}

%post -n libmsi1 -p /sbin/ldconfig
%postun -n libmsi1 -p /sbin/ldconfig

%files
%doc COPYING NEWS README TODO
%{_bindir}/msibuild
%{_bindir}/msidiff
%{_bindir}/msidump
%{_bindir}/msiextract
%{_bindir}/msiinfo
%{_bindir}/wixl
%{_bindir}/wixl-heat
%{_datadir}/bash-completion/completions/msitools
%{_datadir}/wixl-%{version}%{?_version_suffix}

%files -n libmsi1 -f %{name}.lang
%doc COPYING.LIB
%{_libdir}/girepository-1.0/Libmsi-1.0.typelib
%{_libdir}/libmsi.so.0*

%files -n libmsi1-devel
%{_datadir}/gir-1.0/Libmsi-1.0.gir
%{_datadir}/vala/vapi/libmsi-1.0.vapi
%{_includedir}/libmsi-1.0/*
%{_libdir}/libmsi.so
%{_libdir}/pkgconfig/libmsi-1.0.pc


%changelog
* Thu Aug 02 2018 Victor Toso <victortoso@redhat.com> - 0.98-1
- Update to 0.98

* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.97-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Mon Jul 31 2017 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.97-2
- Add a patch to fix spice packaging

* Mon Jul 31 2017 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.97-1
- Upstream release 0.97

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.96-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.96-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Wed Nov 09 2016 Fabiano Fidêncio <fidencio@redhat.com> - 0.96-1

* Wed Jun 22 2016 Fabiano Fidêncio <fidencio@redhat.com> - 0.95-5
- Add libpcre as glib dependency

* Mon May 02 2016 Fabiano Fidêncio <fidencio@redhat.com> - 0.95-4
- Fix libvirt-glib's translations
- Add adwaita-icon-theme

* Tue Feb 23 2016 Fabiano Fidêncio <fidencio@redhat.com> - 0.95-3
- Add libvirt-glib.wxi
- Update nettle to 3.2.1

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.95-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Dec 01 2015 Fabiano Fidêncio <fidencio@redhat.com> - 0.95-1
- Upstream release 0.95

* Fri Jun 19 2015 Fabiano Fidêncio <fidencio@redhat.com> - 0.94-2
- Add libepoxy.wxi
- Add libepoxy as gtk3 dep

* Tue Jun 16 2015 Fabiano Fidêncio <fidencio@redhat.com> - 0.94-1
- Upstream release 0.94

* Mon Jan  5 2015 Fabiano Fidêncio <fidencio@redhat.com> - 0.93.75-2
- Update spice-glib.wxi to use gstreamer1

* Wed Dec 10 2014 Fabiano Fidêncio <fidencio@redhat.com> - 0.93.75-1
- Update to upstream msitool-0.93.75-f89b

* Tue Oct  7 2014 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.93.43-1
- Set the ALLUSERS=1 property when the install scope is perMachine, rhbz#1146586

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.93.40-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Tue Jul 22 2014 Kalev Lember <kalevlember@gmail.com> - 0.93.40-2
- Rebuilt for gobject-introspection 1.41.4

* Mon Jun  9 2014 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.93.40-1
- Update nettle.wxi for f20, rhbz#1106437

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.93.28-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon Feb 10 2014 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.93.28-1
- Update libvirt.wxi for f20

* Tue Dec 17 2013 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.93.24-1
- Update libpng for f20

* Sat Nov  9 2013 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.93.20-1
- Fix component id stability
- Fix libmsi crash, rhbz#1027256

* Tue Aug 13 2013 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.93.6-1
- Update to upstream 0.93.6-74a2, fixes RegLocator

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.93-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Jul 11 2013 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.93-1
- Upstream release 0.93

* Fri Apr 12 2013 Marc-André Lureau <marcandre.lureau@gmail.com> - 0.92.26-1
- 0.92.26 snapshot from upstream, fix f19 includes
- Add x64 build support

* Thu Apr 11 2013 Marc-André Lureau <marcandre.lureau@gmail.com> - 0.92-5
- Forgot a patch

* Fri Apr  5 2013 Marc-André Lureau <marcandre.lureau@gmail.com> - 0.92-4
- More f19 wxi updates

* Thu Apr  4 2013 Marc-André Lureau <marcandre.lureau@gmail.com> - 0.92-3
- Remove obsolete file in glib.wxi

* Wed Apr  3 2013 Marc-André Lureau <marcandre.lureau@gmail.com> - 0.92-2
- Add a glib.wxi workaround for win64

* Thu Mar  7 2013 Marc-André Lureau <marcandre.lureau@gmail.com> - 0.92-1
- New upstream release.
- Add msidump & msidiff tools.
- Add translations.

* Fri Feb 15 2013 Paolo Bonzini <pbonzini@redhat.com> - 0.91-3
- Add dependency of libmsi1-devel on libmsi1, reformatted descriptions.

* Thu Feb 14 2013 Paolo Bonzini <pbonzini@redhat.com> - 0.91-2
- Remove invalid characters from package names, added vala-tools
  BuildRequires.

* Mon Jan 28 2013 Paolo Bonzini <pbonzini@redhat.com> - 0.91-1
- New upstream version.

* Wed Jan 16 2013 Paolo Bonzini <pbonzini@redhat.com> - 0.90-1
- Added wixl and devel packages.

* Thu Dec 6 2012 Paolo Bonzini <pbonzini@redhat.com> - 0.01-1
- Initial version.
